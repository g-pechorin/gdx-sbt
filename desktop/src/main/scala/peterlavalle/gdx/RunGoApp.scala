package peterlavalle.gdx

import java.io.InputStream

object RunGoApp extends App {
  val stream: InputStream = getClass.getResourceAsStream("/gdx64.dll")
  if (null == stream)
    sys.error("didn't find the dll")
  println("it found? " + getClass.getResource("/gdx64.dll"))

    com.mygdx.game.desktop.DesktopLauncher.mainn(args)

}
