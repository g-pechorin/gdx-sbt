package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.MyGdxGame;

import javax.swing.*;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;

public class SwingLauncher {
    public static void mainn(String[] args) throws InterruptedException, InvocationTargetException {
        SwingUtilities.invokeAndWait(
                new Runnable() {
                    @Override
                    public void run() {
                        MyGdxGame myGdxGame = new MyGdxGame();

                        LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
                        configuration.useGL30 = true;

                        final LwjglAWTCanvas awtCanvas = new LwjglAWTCanvas(myGdxGame, configuration);

                        Canvas canvas = awtCanvas.getCanvas();
                        canvas.setMinimumSize(new Dimension(320, 240));

                        JTree tree = new JTree();

                        JFrame editor = new JFrame("the editor");
                        editor.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
                        editor.setContentPane(new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, tree, canvas));
                        editor.setVisible(true);
                        editor.pack();
                    }
                }
        );
    }
}
