import sbt.Keys.libraryDependencies


organization := "com.peterlavalle"

resolvers += Classpaths.typesafeReleases
resolvers += Resolver.mavenCentral
resolvers += Resolver.jcenterRepo
resolvers += "jitpack" at "https://jitpack.io"

resolvers += Resolver.mavenLocal
// resolvers += Resolver.google()
resolvers += Resolver.sonatypeRepo("releases")
//     gradlePluginPortal()
//     maven { url "https://oss.sonatype.org/content/repositories/snapshots/" }
//     maven { url "https://oss.sonatype.org/content/repositories/releases/" }
// }

val all = Seq(
  resolvers += Classpaths.typesafeReleases,
  resolvers += Resolver.mavenCentral,
  resolvers += Resolver.jcenterRepo,
  resolvers += "jitpack" at "https://jitpack.io",
  resolvers += Resolver.mavenLocal,
  resolvers += Resolver.sonatypeRepo("releases"),
)
val gdxVersion = "1.10.0"



lazy val root =
  (project in file("."))
    .aggregate(
      core,
      desktop,
    )

lazy val core = project
  .settings(all : _ *)
  .settings(
    libraryDependencies += "com.badlogicgames.gdx" % "gdx" % gdxVersion,
    Compile / javaSource := baseDirectory.value.getParentFile / "gradle-example/core/src",
  )

lazy val desktop = project
  .dependsOn(core)
  .settings(all : _ *)
  .settings(
      libraryDependencies ++= Seq(
      "com.gitlab.g-pechorin" % "gdxjit" % "77cc152"
//      https://jitpack.io/com/gitlab/g-pechorin/gdxjit/77cc152/
  //    "com.badlogicgames.gdx" % "gdx-backend-lwjgl" % gdxVersion,
    //  "com.badlogicgames.gdx" % "gdx-tools" % gdxVersion,
      //"com.badlogicgames.gdx" % "gdx-platform" % gdxVersion,
    ),

    // i want this https://repo1.maven.org/maven2/com/badlogicgames/gdx/gdx-platform/1.10.0/gdx-platform-1.10.0-natives-desktop.jar
    //    libraryDependencies += "com.badlogicgames.gdx" % "gdx-platform" % gdxVersion
    //      artifacts Artifact("natives-desktop"),
    //    classpathTypes ++= Set("natives-desktop"),
//    Compile / unmanagedJars := ((baseDirectory.value / "lib" )** ".jar").classpath,
    Compile / resourceDirectory := baseDirectory.value.getParentFile / "gradle-example/core/assets",

    Compile / javaSource := baseDirectory.value.getParentFile / "gradle-example/desktop/src"
  )
